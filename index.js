const CORS_PROXY = 'https://api.codetabs.com/v1/proxy?quest='
const BASE_URL = 'https://www.metaweather.com/api/location/'
const ICONS_URL = 'https://www.metaweather.com/static/img/weather/ico/'
const title = document.getElementById('title')
const date = document.getElementById('date')
const form = document.getElementById('form')
const formButton = document.getElementById('search-submit')
const previousButton = document.getElementById('previous-search')
const iconContainer = document.getElementById('icon-container')
const iconDescription = document.getElementById('icon-description')
const cityInput = document.getElementById('search-input')
const currentTemp = document.getElementById('current-temp')
const errorMsg = document.getElementById('msg')
let data

//Commands to be executed when the page first loads
window.onload = () => {
  window.localStorage.setItem('lastSearch', '')
  cityInput.value = 'San Francisco'
  fetchData('san francisco', setFields)
  setTimeout(function () {
    document.getElementById('ajax-container').classList.add('visible')
  }, 2000)
}

//Helper function to debounce another function
const debounce = (func, wait) => {
  let timeout

  return function executedFunction(...args) {
    const later = () => {
      clearTimeout(timeout)
      func(...args)
    }

    clearTimeout(timeout)
    timeout = setTimeout(later, wait)
  }
}

//Helper function to throttle another function
const throttle = (func, limit) => {
  let inThrottle
  return function () {
    const args = arguments
    const context = this
    if (!inThrottle) {
      func.apply(context, args)
      inThrottle = true
      setTimeout(() => (inThrottle = false), limit)
    }
  }
}

//Helper function that throttles the calls to fetch the suggested cities for the current input
const getSuggestions = throttle(() => {
  if (cityInput.value != '') {
    fetchSuggestions()
  }
}, 2000)

//Event listeners for the submit action and the click action for the submit button
form.addEventListener('submit', (e) => {
  e.preventDefault()
  $('#search-submit').click()
})

formButton.addEventListener(
  'click',
  debounce(() => {
    if (checkInput()) {
      fetchData(cityInput.value, setFields)
    }
  }, 2000)
)

previousButton.addEventListener(
  'click',
  debounce(() => {
    if (checkInput()) {
      cityInput.value = window.localStorage.getItem('lastSearch');
      fetchData(window.localStorage.getItem('lastSearch'), setFields)
    }
  }, 2000)
)

cityInput.addEventListener('keyup', getSuggestions)

//Function that fetches the suggested cities for the current input and shows them using autocomplete
function fetchSuggestions() {
  return $(function () {
    async function sourceData() {
      let res = await fetch(
        CORS_PROXY + BASE_URL + 'search/?query=' + cityInput.value
      )
      suggestionData = await res.json()
      suggestions = []

      for (let i = 0; i < suggestionData.length; i++) {
        suggestions.push(suggestionData[i].title)
      }

      return suggestions
    }

    sourceData().then((val) =>
      $('#search-input').autocomplete(
        {
          source: val,
          select: function (event, ui) {
            $('#search-input').val(ui.item.label)
            $('#search-submit').click()
          },
        },
        {
          delay: 0,
          minLength: 1,
        }
      )
    )
  })
}

//Fetches the data from the Weather API and stores it in the variable 'data'
async function fetchData(city, callback) {
  if (data) {
    window.localStorage.setItem('lastSearch', data.title)
  }

  let res = await fetch(CORS_PROXY + BASE_URL + 'search/?query=' + city)
  data = await res.json()

  res = await fetch(CORS_PROXY + BASE_URL + `${data[0].woeid}`)
  data = await res.json()

  callback()

  if (window.localStorage.getItem('lastSearch') != '') {
    previousButton.style.visibility = 'visible';
  }
}

//Creates and formats the today's date to '<DayName>, <Date> <Month> <Year>'
function getTodaysDate() {
  const days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ]
  const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ]
  const today = new Date()

  return `${days[today.getDay()]}, ${today.getDate()} ${
    months[today.getMonth()]
  } ${today.getFullYear()}`
}

//Sets the corresponding data to each field
function setFields() {
  //Sets the fields for 'location-date'
  title.innerHTML = `${data.title}, ${data.parent.title}`
  date.innerHTML = getTodaysDate()

  //Sets the fields for 'current-general'
  //Checks if there is already an icon in current-general
  if (iconContainer.childElementCount == 2) {
    iconContainer.firstChild.remove()
  }

  iconContainer.insertAdjacentHTML(
    'afterbegin',
    `<img src="${
      ICONS_URL + data.consolidated_weather[0].weather_state_abbr + '.ico'
    }" class="current-general__icon">`
  )
  iconDescription.innerHTML = data.consolidated_weather[0].weather_state_name
  currentTemp.innerHTML = `${Math.round(
    data.consolidated_weather[0].the_temp
  )}&deg`

  //Sets the fields for 'current-stats'
  setCurrentStats()

  //Sets the fields for 'next-5'
  setNext5Days()
}

//Sets the values and corresponding labels for the stats of the current city
function setCurrentStats() {
  const currentStatsValues = document.querySelectorAll('.current-stats__value')
  const currentStatsLabels = document.querySelectorAll('.current-stats__label')

  for (let i = 0; i < currentStatsValues.length; i++) {
    switch (i) {
      case 0:
        currentStatsValues[0].innerHTML = `${Math.round(
          data.consolidated_weather[0].max_temp
        )}&deg`
        currentStatsLabels[0].innerHTML = 'High'
        break

      case 1:
        currentStatsValues[1].innerHTML = `${Math.round(
          data.consolidated_weather[0].min_temp
        )}&deg`
        currentStatsLabels[1].innerHTML = 'Low'
        break

      case 2:
        currentStatsValues[2].innerHTML = `${Math.round(
          data.consolidated_weather[0].wind_speed
        )}mph`
        currentStatsLabels[2].innerHTML = 'Wind'
        break

      case 3:
        currentStatsValues[3].innerHTML = `${Math.round(
          data.consolidated_weather[0].humidity
        )}%`
        currentStatsLabels[3].innerHTML = 'Humidity'
        break

      case 4:
        currentStatsValues[4].innerHTML = data.sun_rise.substring(11, 16)
        currentStatsLabels[4].innerHTML = 'Sunrise'
        break

      case 5:
        currentStatsValues[5].innerHTML = data.sun_set.substring(11, 16)
        currentStatsLabels[5].innerHTML = 'Sunset'
        break
    }
  }
}

//Sets the values and labels for the next 5 days
function setNext5Days() {
  const heading = document.querySelector('.next-5__heading')
  const rows = document.querySelectorAll('.next-5__row')
  const next5days = [...data.consolidated_weather]
  next5days.shift()

  heading.innerHTML = 'Next 5 Days'

  for (let i = 0; i < next5days.length; i++) {
    let d = new Date(next5days[i].applicable_date + ' 00:00:00')
    let dateHTML = `<div class="next-5__date"> ${String(d).substring(
      0,
      3
    )} <div class="next-5__label">${String(d.getDate())}/${String(
      d.getMonth() + 1
    )}</div></div>`
    let lowHTML = `<div class="next-5__low"> ${Math.round(
      next5days[i].min_temp
    )}&deg <div class="next-5__label">Low</div></div>`
    let highHTML = `<div class="next-5__high"> ${Math.round(
      next5days[i].max_temp
    )}&deg; <div class="next-5__label">High</div></div>`
    let iconHTML = `<div class="next-5__icon"> <img src="${
      ICONS_URL + next5days[i].weather_state_abbr + '.ico'
    }"> <div class="next-5__label">${
      next5days[i].weather_state_name
    }</div></div>`
    let rainHTML = `<div class="next-5__rain"> ${Math.round(
      next5days[i].humidity
    )}% <div class="next-5__label">Humidity</div></div>`
    let windHTML = `<div class="next-5__wind"> ${Math.round(
      next5days[i].wind_speed
    )}mph <div class="next-5__label">Wind</div></div>`

    rows[i].innerHTML = ''

    rows[i].insertAdjacentHTML('afterbegin', windHTML)
    rows[i].insertAdjacentHTML('afterbegin', rainHTML)
    rows[i].insertAdjacentHTML('afterbegin', iconHTML)
    rows[i].insertAdjacentHTML('afterbegin', highHTML)
    rows[i].insertAdjacentHTML('afterbegin', lowHTML)
    rows[i].insertAdjacentHTML('afterbegin', dateHTML)
  }
}

function checkInput() {
  if (cityInput.value == '') {
    errorMsg.innerHTML = '*Please enter a city name'
    return false
  }

  errorMsg.innerHTML = ''
  return true
}
